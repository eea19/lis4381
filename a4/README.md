> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web App Development

## Elka Anistratenko

### Assignment 4 Requirements:

*Three Parts*:

1. Create Local LIS4381 Online Portfolio using Apache and Bootstrap
    * Provide screenshot of portfolio homepage 
    * Provide link to full portfolio
2. Use jQuery to validate client side data
    * Attach screenshots of failed validation and successful validation
3. Skill Sets 10-12

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
>

#### A4 Screenshots:

*Link to Local LIS4381 Online Portfolio*:

[LIS4381 Local Online Portfolio](http://localhost/repos/lis4381/index.php)

*Screenshot of Local LIS4381 Online Portfolio*:

Portfolio Home Screen: | Failed Data Validation: | Valid Data Entry:
--- | --- | ---
![Portfolio Home](img/home.png) | ![Failed Validation](img/failed_dv.png) | ![Valid Entry](img/passed_dv.png)

*Screenshot of Python Skill Sets*:

Skill Set 10: Array List | Skill Set 11: Alpha Numeric Special | Skill Set 12: Temperature Conversion Program
--- | --- | ---
![Skill Set 10 Array List Screenshot](img/10_array_list.png) | ![Skill Set 11 Alpha Numeric Special Screenshot](img/11_alpha_num_special.png) | ![Skill Set 12 Temperature Conversion Program Screenshot](img/12_temp_conversion_prog.png)