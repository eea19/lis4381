> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web App Development

## Elka Anistratenko

### Assignment 3 Requirements:

*Three Parts*:

1. Create ERD
2. Create "My Event" Application
3. Provide links to a3.mwb and a3.sql

#### README.md file should include the following items:

* Screenshot of Pet Store ERD
* Screenshot of Concert Ticket first and second user interface
* Screen shots of 10 records from each Pet Store table
* Links to a3.mwb and a3.sql


#### Assignment Screenshots:
| Screenshot 1 of Concert Ticket App | Screenshot 2 of Concert Ticket App |
| -----------------------------------| ----------------------------------- |
| ![Android Studio "Concert Tickets" Screenshot 1](img/a3_1.png) | ![Android Studio "Concert Tickets" Screenshot 2](img/a3_2.png) |

##### Screenshot of Pet Store ERD
![Pet Store ERD](img/a3_erd.png)

#### Screenshot of 10 records from each Pet Store table

*Pet Store Table*
![Records of Pet Store Table](img/petstore.png)

*Pet Table*
![Records of Pet Table](img/pet.png)

*Customer Table*
![Records of Customer Table](img/customer.png)


#### Links to a3.mwb and a3.sql files

[a3.sql file](https://bitbucket.org/eea19/lis4381/src/master/a3/a3.sql)

[a3.mwb file](https://bitbucket.org/eea19/lis4381/src/master/a3/a3.mwb)

##### Skill Set 4: Decision Structures

![Skill Set 4 Decision Structures Screenshot](img/4_decision_structures.png)

##### Skill Set 5: Random Number Generator

![Skill Set 5 Random Number Generator Screenshot](img/5_random_num_gen.png)

##### Skill Set 6: Methods

![Skill Set 6 Methods Screenshot](img/6_methods.png)