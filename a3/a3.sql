SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE=`ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION`;

-- -----------------------------------------------------
-- Schema eea19
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `eea19`;

-- -----------------------------------------------------
-- Schema eea19
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `eea19` DEFAULT CHARACTER SET utf8mb4 ;
SHOW WARNINGS;
USE `eea19` ;

-- -----------------------------------------------------
-- Table `eea19`.`petstore`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eea19`.`petstore` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `eea19`.`petstore` (
  `pst_id` SMALLINT NOT NULL AUTO_INCREMENT,
  `pst_name` VARCHAR(30) NOT NULL,
  `pst_street` VARCHAR(30) NOT NULL,
  `pst_city` VARCHAR(30) NOT NULL,
  `pst_state` CHAR(2) NOT NULL,
  `pst_zip` CHAR(9) NOT NULL,
  `pst_phone` BIGINT UNSIGNED NOT NULL,
  `pst_email` VARCHAR(100) NOT NULL,
  `pst_url` VARCHAR(100) NOT NULL,
  `pst_ytd_sales` DECIMAL(10,2) NOT NULL,
  `pst_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pst_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `eea19`.`customer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eea19`.`customer` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `eea19`.`customer` 
(
  `cus_id` SMALLINT NOT NULL AUTO_INCREMENT,
  `cus_fname` VARCHAR(15) NOT NULL,
  `cus_lname` VARCHAR(30) NOT NULL,
  `cus_street` VARCHAR(30) NOT NULL,
  `cus_city` VARCHAR(30) NOT NULL,
  `cus_state` CHAR(2) NOT NULL,
  `cus_zip` CHAR(9) NOT NULL,
  `cus_phone` BIGINT UNSIGNED NOT NULL,
  `cus_email` VARCHAR(100) NOT NULL,
  `cus_balance` DECIMAL(8,2) NOT NULL,
  `cus_total_sales` DECIMAL(8,2) NOT NULL,
  `cus_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`cus_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `eea19`.`pet`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eea19`.`pet`;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `eea19`.`pet` 
(
  `pet_id` SMALLINT NOT NULL AUTO_INCREMENT,
  `pst_id` SMALLINT NOT NULL,
  `cus_id` SMALLINT NULL,
  `pet_type` VARCHAR(45) NOT NULL,
  `pet_sex` ENUM('m','f') NOT NULL,
  `pet_cost` DECIMAL(6,2) NOT NULL,
  `pet_price` DECIMAL(6,2) NOT NULL,
  `pet_age` TINYINT NOT NULL,
  `pet_color` VARCHAR(30) NOT NULL,
  `pet_sale_date` DATE NULL,
  `pet_vaccine` ENUM('y', 'n') NOT NULL,
  `pet_neuter` ENUM('y', 'n') NOT NULL,
  `pet_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pet_id`),
  INDEX `fk_pet_petstore_idx` (`pst_id` ASC) VISIBLE,
  INDEX `fk_pet_customer1_idx` (`cus_id` ASC) VISIBLE,
  CONSTRAINT `fk_pet_petstore0`
    FOREIGN KEY (`pst_id`)
    REFERENCES `eea19`.`petstore` (`pst_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_pet_customer10`
    FOREIGN KEY (`cus_id`)
    REFERENCES `eea19`.`customer` (`cus_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `eea19`.`petstore`
-- -----------------------------------------------------
START TRANSACTION;
USE `eea19`;
INSERT INTO `eea19`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Dog Days', '3 Porter Drive', 'Amarillo', 'TX', '791591234', 8064422458, 'dogdays@gmail.com', 'dogdays.com', 928.03, NULL);
INSERT INTO `eea19`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Raining Cats and Dogs', '9726 Center Court', 'York', 'PA', '213578907', 7175496992, 'rainingcatsdogs@gmail.com', 'rainingcatsdogs.com', 346.71, NULL);
INSERT INTO `eea19`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Pup Palace', '468 Briar Crest Terrace', 'Madison', 'WI', '537056789', 6089329505, 'puppalace@gmail.com', 'puppalace.com', 210.44, NULL);
INSERT INTO `eea19`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'My Dawgs', '33 Crest Line Way', 'Decatur', 'IL', '625254567', 2172382224, 'mydawgs@gmail.org', 'mydawgs.com', 673.61, NULL);
INSERT INTO `eea19`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Pet Vacay', '194 Vera Way', 'El Paso', 'TX', '799683546', 9158885799, 'petvacay@gmail.com', 'petvacay.com', 941.68, NULL);
INSERT INTO `eea19`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'PetSmart', '60956 Oriole Drive', 'Nashville', 'TN', '372286904', 6153724101, 'petsmart@gmail.com', 'petsmart.com', 781.35, NULL);
INSERT INTO `eea19`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'PetCo', '904 Orin Crossing', 'Nashville', 'TN', '372352468', 6155266697, 'petco@gmail.com', 'petco.com', 49.87, NULL);
INSERT INTO `eea19`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Paw Place', '50 Mockingbird Junction', 'Houston', 'TX', '770707987', 8327500641, 'pawplace@gmail.com', 'pawplace.com', 587.35, NULL);
INSERT INTO `eea19`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Furry Friends', '12188 Ridge Oak Parkway', 'Florence', 'SC', '295052356', 8434096942, 'furryfriends@gmail.com', 'furryfriends.com', 164.62, NULL);
INSERT INTO `eea19`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Fur-Real Friends', '66 Forest Hill', 'Albany', 'NY', '122427809', 5185386991, 'furrealfriends@gmail.com', 'furrealfriends.com', 31.33, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `eea19`.`customer`
-- -----------------------------------------------------
START TRANSACTION;
USE `eea19`;
INSERT INTO `eea19`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'James', 'Campes', '25857 Loftsgordon Trail', 'Lawrenceville', 'GA', '300451234', 7703466394, 'person1@gmail.com', 251.42, 7472.15, NULL);
INSERT INTO `eea19`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Justin', 'Regan', '5 Huxley Alley', 'Juneau', 'AK', '998122345', 9079029516, 'person2@gmail.com', 1779.55, 6671.08, NULL);
INSERT INTO `eea19`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Jennifer', 'Clappson', '97 Corscot Road', 'Virginia Beach', 'VA', '234714356', 7571135143, 'person3@gmail.com', 1767.57, 2268.85, NULL);
INSERT INTO `eea19`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Penelope', 'Gresswood', '93 Westport Center', 'Herndon', 'VA', '220703456', 7574063582, 'person4@gmail.com', 9516.44, 6599.25, NULL);
INSERT INTO `eea19`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Henry', 'Bemrose', '70 Meadow Ridge Crossing', 'Atlanta', 'GA', '303863456', 4046912093, 'person5@gmail.com', 7918.31, 9230.17, NULL);
INSERT INTO `eea19`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Kayla', 'Ambrogetti', '40229 Dakota Drive', 'Phoenix', 'AZ', '850454321', 4808822075, 'person6@gmail.com', 8014.32, 2068.43, NULL);
INSERT INTO `eea19`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Peter', 'Oven', '469 West Circle', 'Baltimore', 'MD', '212395432', 4438512885, 'person7@gmail.com', 1017.11, 1540.02, NULL);
INSERT INTO `eea19`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Yenny', 'Fawloe', '3939 Upham Junction', 'Boise', 'ID', '837167653', 2083567674, 'person8@gmail.com', 7658.01, 2124.12, NULL);
INSERT INTO `eea19`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Wilbur', 'Cressor', '15275 Cardinal Place', 'Fresno', 'CA', '937269403', 2096560679, 'person9@gmail.com', 9507.69, 4723.89, NULL);
INSERT INTO `eea19`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Cassidy', 'Palmer', '2 Manley Junction', 'Richmond', 'VA', '232604839', 8041642310, 'person10@gmail.com', 5709.17, 6870.09, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `eea19`.`pet`
-- -----------------------------------------------------
START TRANSACTION;
USE `eea19`;
INSERT INTO `eea19`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (1, 2, NULL, 'Doberman', 'm', 267.99, 510.2, 16, 'Black', '2021-01-21', 'y', 'n', NULL);
INSERT INTO `eea19`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (2, 3, NULL, 'Chiwawa', 'f', 851.98, 715.95, 30, 'Green', '2020-03-21', 'y', 'n', NULL);
INSERT INTO `eea19`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (3, 4, 2, 'American Bobtail', 'm', 755.17, 495.86, 3, 'Beige', '2019-03-23', 'y', 'y', NULL);
INSERT INTO `eea19`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (4, 5, NULL, 'American Longhair', 'f', 196.08, 858.62, 7, 'Brown', '2021-05-05', 'n', 'y', NULL);
INSERT INTO `eea19`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (5, 6, NULL, 'British Shorthair', 'm', 593.95, 541.05, 29, 'Brown', '2021-10-02', 'n', 'n', NULL);
INSERT INTO `eea19`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (6, 7, 5, 'Parakeet', 'f', 275.24, 823, 23, 'Yellow', NULL, 'n', 'n', NULL);
INSERT INTO `eea19`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (7, 8, 4, 'African Parrot', 'f', 255.94, 588.4, 17, 'Blue/Green/Red', NULL, 'n', 'y', NULL);
INSERT INTO `eea19`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (8, 9, NULL, 'Pitbull', 'f', 962.02, 763.22, 4, 'White', NULL, 'y', 'y', NULL);
INSERT INTO `eea19`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (9, 10, 7, 'Cocker Spaniel', 'm', 372.52, 307.8, 22, 'Red', '2019-01-20', 'n', 'n', NULL);
INSERT INTO `eea19`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (10, 1, NULL, 'Giraffe', 'm', 111.2, 727.19, 14, 'Orange/Brown', '2018-03-15', 'y', 'n', NULL);

COMMIT;

select * from petstore;
select * from customer;
select * from pet;

