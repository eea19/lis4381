<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Use MySQL Workbench to create a Pet Store Database and ERD. Use Android Studio to create Concert Ticket App.">
		<meta name="author" content="Elka Anistratenko, IT Major">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment 3</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> 
					<p class="text-justify">* Screenshot of Pet Store ERD</p>
					<p class="text-justify">* Screenshot of Concert Ticket first and second user interface</p>
					<p class="text-justify">* Screen shots of 10 records from each Pet Store table</p>
					<p class="text-justify">* Links to a3.mwb and a3.sql </p>
					<p class="text-justify">* Skill sets 4-6 </p>
				</p>

				<h4>Screenshot 1 of Concert Ticket App</h4>
				<img src="img/a3_1.png" class="img-responsive center-block" alt="JDK Installation">

				<h4>Screenshot 2 of Concert Ticket App</h4>
				<img src="img/a3_2.png" class="img-responsive center-block" alt="Android Studio Installation">

				<h4>Screenshot of Pet Store ERD</h4>
				<img src="img/a3_erd.png" class="img-responsive center-block" alt="AMPPS Installation">

				<h4>Links to a3.mwb and a3.sql files</h4>
				<p><?php echo "<a href='https://bitbucket.org/eea19/lis4381/src/master/a3/a3.sql'>a3.sql file</a>" ; ?></p>
				<p><?php echo "<a href='https://bitbucket.org/eea19/lis4381/src/master/a3/a3.mwb'>a3.mwb file</a>" ; ?></p>

				<h4>10 Records from Pet Store Table</h4>
				<img src="img/petstore.png" class="img-responsive center-block" alt="AMPPS Installation">

				<h4>10 Records from Pet Table</h4>
				<img src="img/pet.png" class="img-responsive center-block" alt="AMPPS Installation">

				<h4>10 Records from Customer Table</h4>
				<img src="img/customer.png" class="img-responsive center-block" alt="AMPPS Installation">

				<h4>Skill Set 4 Decision Structures Screenshot</h4>
				<img src="img/4_decision_structures.png" class="img-responsive center-block" alt="AMPPS Installation">

				<h4>Skill Set 5 Random Number Generator Screenshot</h4>
				<img src="img/5_random_num_gen.png" class="img-responsive center-block" alt="AMPPS Installation">

				<h4>Skill Set 6 Methods Screenshot</h4>
				<img src="img/6_methods.png" class="img-responsive center-block" alt="AMPPS Installation">
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
