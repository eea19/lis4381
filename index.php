<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Elka Anistratenko's LIS4381 Mobile Web Applications Portfolio">
		<meta name="author" content="Elka Anistratenko, IT Major">
		<link rel="icon" href="favicon.ico">

		<title>Elka Anistratenko's LIS4381 Portfolio</title>

		<?php include_once("css/include_css.php"); ?>	

		<!-- Carousel styles -->
		<style type="text/css">
		 h2
		 {
			 margin: 0;     
			 color: #666;
			 padding-top: 50px;
			 font-size: 52px;
			 font-family: "trebuchet ms", sans-serif;    
		 }
		 p
		 {
			 color: black;
			 font-size: 26px;
			 padding-top: 10px;
			 padding-bottom: 0px
		 }
		 h3
		 {
			 color: rgb(106,90,205);
			 text-decoration: underline;
			 font-size: 34px;
			 margin:0px 0px 5px 0px;
		 }
		 h4
		 {
			font-size: 28px;
			margin:0px 0px 0px 0px;
		 }
		 .item
		 {
			 background: #333;    
			 text-align: center;
			 height: 300px !important;
		 }
		 .carousel
		 {
			 margin: 20px 0px 20px 0px;
		 }
		 .bs-example
		 {
			 margin: 20px;
		 }
		 .btn
		 {
			padding-top: 0px;
			padding-bottom: 0px;
		 }
		</style>

	</head>
	<body>

		<?php include_once("global/nav_global.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>

				<!-- Start Bootstrap Carousel  -->
				<div class="bs-example">
					<div
						id="myCarousel"
								class="carousel"
								data-interval="1000"
								data-pause="hover"
								data-wrap="true"
								data-keyboard="true"			
								data-ride="carousel">
						
    				<!-- Carousel indicators -->
						<ol class="carousel-indicators">
							<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
							<li data-target="#myCarousel" data-slide-to="1"></li>
							<li data-target="#myCarousel" data-slide-to="2"></li>
						</ol>   
						<!-- Carousel items -->
						<div class="carousel-inner">

							<!-- -Note: you will need to modify the code to make it work with *both* text and images.  -->
							<div class="active item" style="background: url(img/slide1.png) no-repeat left center; background-size: cover;">
								<div class="container">
									<div class="carousel-caption">
										<h4>Thank you for visiting...</h4>
										<h3>Elka's LIS4381 Portfolio!</h3>
										<p class="lead">This is a summary of skills Elka Anistratenko has acquired through completion of assignments in Mobile Web Application Development as a BSIT student at FSU.</p>
										<a class="btn btn-large btn-info" href="https://www.linkedin.com/in/elka-anistratenko-1a336b22b/">See more</a>
                        </div>
                      </div>
                    </div>
              
							<div class="item">
							<img src="img/slide2.png" alt="Slide 2">
								<h2>Slide 2</h2>
								<div class="carousel-caption">
									<h3><b>BitBucket Repositories</b></h3>
									<p>This is a preview of my remote repositories.</p>
									<a class="btn btn-large btn-info" href="https://bitbucket.org/eea19/workspace/projects/">Go to Repos!</a>									
								</div>
							</div>

							<div class="item">
							<img src="img/slide3.png" alt="Slide 3">
								<h2>Slide 3</h2>
								<div class="carousel-caption">
									<h3><b>BitBucket Repositories</b></h3>
									<p>This is a preview of some recent commits on my LIS4381 remote repository.</p>
									<a class="btn btn-large btn-info" href="https://bitbucket.org/eea19/lis4381/src/master/">Go to LIS4381!</a>									
								</div>
							</div>

						</div>
						<!-- Carousel nav -->
						<a class="carousel-control left" href="#myCarousel" data-slide="prev">
							<span class="glyphicon glyphicon-chevron-left"></span>
						</a>
						<a class="carousel-control right" href="#myCarousel" data-slide="next">
							<span class="glyphicon glyphicon-chevron-right"></span>
						</a>
					</div>
				</div>
				<!-- End Bootstrap Carousel  -->
				
				<?php
				include_once "global/footer.php";
				?>

			</div> <!-- end starter-template -->
    </div> <!-- end container -->

		<?php include_once("js/include_js.php"); ?>	
	  
  </body>
</html>
