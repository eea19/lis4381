<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Use Android Studio to create My Business Card Application.">
		<meta name="author" content="Elka Anistratenko, IT Major">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Project 1</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> 
					<p class="text-justify">* Create "My Business Card" Application</p>
					<p class="text-justify">* Provide screenshots of both app interfaces</p>
					<p class="text-justify">* Skill sets 7-9 </p>
				</p>

				<h4>Screenshot 1 of My Business Card App</h4>
				<img src="img/p1_1.png" class="img-responsive center-block" alt="Android Studio My Business Card App Screenshot 1">

				<h4>Screenshot 2 of My Business Card App</h4>
				<img src="img/p1_2.png" class="img-responsive center-block" alt="Android Studio My Business Card App Screenshot 2">

				<h4>Skill Set 7: Random Num Generator (w/ data validation)</h4>
				<img src="img/ss7.png" class="img-responsive center-block" alt="Skill Set 7 Random Num Generator (w/ data validation) Screenshot">

				<h4>Skill Set 8: Largest of Three Numbers</h4>
				<img src="img/ss8.png" class="img-responsive center-block" alt="Skill Set 8 Largest of Three Numbers Screenshot">

				<h4>Skill Set 9: Array Runtime Data Validation</h4>
				<img src="img/ss9.png" class="img-responsive center-block" alt="Skill Set 9 Array Runtime Data Validatio Screenshot">
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
