> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web App Development

## Elka Anistratenko

### Project 1 Requirements:

*Three Parts*:

1. Create "My Business Card" Application
2. Provide screenshots of both app interfaces
3. Skill sets 7-9

#### Assignment Screenshots:
| Screenshot 1 of My Business Card App | Screenshot 2 of My Business Card App |
| -----------------------------------| ----------------------------------- |
| ![Android Studio "Business Card" Screenshot 1](img/p1_1.png) | ![Android Studio "Business Card" Screenshot 2](img/p1_2.png) |

##### Skill Set 7: Random Num Generator (w/ data validation)

![Skill Set 7 Random Num Generator (w/ data validation) Screenshot](img/ss7.png)

##### Skill Set 8: Largest of Three Numbers

![Skill Set 8 Largest of Three Numbers Screenshot](img/ss8.png)

##### Skill Set 9: Array Runtime Data Validation

![Skill Set 9 Array Runtime Data Validatio Screenshot](img/ss9.png)