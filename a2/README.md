> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web App Development

## Elka Anistratenko

### Assignment 2 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Create HealthyRecipes app on Android Studio
    - change background and text colors
3. Chapter 3 and 4 Questions
4. Skill sets 1-3

#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per A1
* Screenshots of running Android Studio - HealthyRecipes
    - Interface 1
    - Interface 2
* Screenshots of skill sets 1-3
* Bitbucket repo link

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshot of Running HealthyRecipes App Interface 1 in Android Studio*:

![Android Studio HealthyRecipes Interface 1 Screenshot](img/healthy_recipes1.png)

*Screenshot of Running HealthyRecipes App Interface 2 in Android Studio*:

![Android Studio HealthyRecipes Interface 2 Screenshot](img/healthy_recipes2.png)

*Screenshots of Skill Sets*

 Even or Odd: | Largest Number: | Arrays and Loops:
--- | --- | ---
![Skill Set 1 Screenshot](img/1_even_or_odd.png) | ![Skill Set 2 Screenshot](img/2_largest_number.png) | ![Skill Set 3 Screenshot](img/3_arrays_and_loops.png)