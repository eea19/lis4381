<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Using Android Studio to create HealthyRecipes App.">
		<meta name="author" content="Elka Anistratenko, IT Major">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment 2</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> 
					<p class="text-justify"> * Course title, your name, assignment requirements, as per A1 </p>
					<p class="text-justify"> * Screenshots of running Android Studio - HealthyRecipes</p>
    				<p class="text-justify"> 	- Interface 1</p>
					<p class="text-justify">  	- Interface 2</p>
					<p class="text-justify"> * Screenshots of skill sets 1-3</p>
					<p class="text-justify"> * Bitbucket repo link </p>
				</p>

				<h4>Screenshot of Running HealthyRecipes App Interface 1 in Android Studio</h4>
				<img src="img/healthy_recipes1.png" class="img-responsive center-block" alt="HealthyRecipes1">

				<h4>Screenshot of Running HealthyRecipes App Interface 2 in Android Studio</h4>
				<img src="img/healthy_recipes2.png" class="img-responsive center-block" alt="HealthyRecipes2">

				<h4>Screenshot of Skill Set 1</h4>
				<img src="img/1_even_or_odd.png" class="img-responsive center-block" alt="Skill Set 1">

				<h4>Screenshot of Skill Set 2</h4>
				<img src="img/2_largest_number.png" class="img-responsive center-block" alt="Skill Set 2">

				<h4>Screenshot of Skill Set 3</h4>
				<img src="img/3_arrays_and_loops.png" class="img-responsive center-block" alt="Skill Set 3">
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
