> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web App Development

## Elka Anistratenko

### Project 2 Requirements:

*Three Parts*:

1. Edit and delete items from database
2. Create edit_petstore.php and edit_petstore_process.php
3. Chapter Questions

#### Assignment Screenshots:

|  *LIS4381 Home*: | *P2 index.php:*
|---|---|
| ![LIS4381 Home](img/home.png)| ![P2 index.php](img/index.png)|

##### Editing Pet Store Record:

|  *edit_petstore.php*: | *edit_petstore.php Failed Validation*:| 
|---|---|
| ![edit_petstore.php](img/failing_validation.png)| ![Invalid Data](img/error.png)|

|  *edit_petstore.php*: | *edit_petstore.php Passed Validation*:| 
|---|---|
| ![edit_petstore.php](img/passing_validation.png)| ![Valid Data](img/successful_edit.png)|

##### Deleting Pet Store Record:

| *Delete Record Prompt*: | *Successfully Deleted Record*: | 
|---|---|
| ![Delete Record Prompt](img/delete_record.png)| ![Successfully Deleted Record](img/successful_delete.png)|

##### RSS Screenshots

|  *RSS Feed*: | 
|-----------------|
|![NASA RSS](img/rss.png)|


[Link to website](http://localhost:8080/lis4381/)