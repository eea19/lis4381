> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Elka Anistratenko

### LIS4381 Requirements:

*Course Assignments*:

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install AMPPS
    - Install JDK
    - Install Android Studio
        * create "My First App" application
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorial (bitbucketstationlocations)
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Provide screenshots of HealthyRecipes app running on Android Studio.
        - must change background and text colors
    - Chapter 3 and 4 Questions
    - Screenshots of skill sets 1-3

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Screenshot of Pet Store ERD
        * 3 additional screenshots of each table (10 records min.) in the ERD.
    - Screenshot of "My Event" App
        * Upon opening
        * After calculation
    - Links to a3.mwb and a3.sql
    - Chapter 5 & 6 Questions
    - Screenshots of skill sets 4-6


4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Create Local LIS4381 Online Portfolio using Apache and Bootstrap
        * Provide screenshot of portfolio homepage 
        * Provide link to full portfolio
    - Use jQuery to validate client side data
        * Attach screenshots of failed validation and successful validation
    - Skill Sets 10-12
    
5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Create Connection to Local Host database to perform server side validation on data entries
    - Modifiy previous Assignment 4 files (meta tags, navigation links, titles, etc) to fit these Requirements.
    - Skill set 13-15

*Course Projects*:

1. [P1 README.md](p1/README.md "My P1 README.md file")
    - Create "My Business Card" Application
    - Provide screenshots of both app interfaces
    - Skill sets 7-9

2. [P2 README.md](p2/README.md "My P2 README.md file")
    - Edit and delete items from database
    - Create edit_petstore.php and edit_petstore_process.php
    - Chapter Questions