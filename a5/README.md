> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web App Development

## Elka Anistratenko

### Assignment 5 Requirements:

*Three Parts*:

1. Connect portfolio website to databse
2. Demonstrate client-side and server-side validation
3. Skillsets 13, 14, & 15

##### README.md file should include the following items:

* Provide Bitbucket read-only access to repos
* Provide screenshots of validation
* Provide Skillsets


#### Assignment Screenshots:

*Client-side validation:*

| Valid Info | Invalid Info |
|---|---|
| ![Valid client validation Screenshot](img/client_valid.png) | ![Invalid client validation Screenshot](img/invalid_client.png) |

*Server-side validation:*

| Error |
|---|
| ![Error server validation screenshot](img/server_error.png) |

#### Skill Set 13: Sphere Volume Calculator

![Sphere Volume Calculator Screenshot](img/13_sphere_vol_calc.png)

#### Skill Set 14: (PHP) Simple Calculator

| Addition index.php | Addition process.php |
|---|---|
|![(PHP) Simple Calculator Addition index.php Screenshot](img/add_index.png)|![(PHP) Simple Calculator Addition process.php Screenshot](img/add_process.png)|

| Division index.php | Division process.php |
|---|---|
|![(PHP) Simple Calculator Division index.php Screenshot](img/div_index.png)|![(PHP) Simple Calculator Division process.php Screenshot](img/div_process.png)|

| Exponentiation index.php | Exponentiation process.php |
|---|---|
|![(PHP) Simple Calculator Exponentiation index.php Screenshot](img/exp_index.png)|![(PHP) Simple Calculator Exponentiation process.php Screenshot](img/exp_process.png)|

#### Skill Set 15: (PHP) Write/Read File

| index.php | process.php |
|---|---|
|![(PHP) Write/Read File index.php Screenshot](img/15_index.png)|![(PHP) Write/Read File process.php Screenshot](img/15_process.png)|